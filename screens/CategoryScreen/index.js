import React, { Component } from 'react';

// Components
import {
    Container, 
    Header, 
    Left, 
    Right, 
    Body, 
    Button, 
    Icon,
    Text, 
    Title, 
    Subtitle,
    View,
} from './../../components';


// Modules
import { 
	ModuleEventListVertical,
	ModuleCategoriesListHorizontal,
	ModuleHeaderScroll,
} from './../../components/modules';


// Fake Data
import { 
	events,
	subcategories,
} from './../../assets/fakeData';



export default class CategoryScreen extends Component {

  	render() {

  		const navigate = this.props.navigation.navigate;
        
        return (

        	<Container>

        		<ModuleHeaderScroll>

                    <View>

				     	<Header hasSubtitle>
							<Left>
								<Button onPress={() => this.props.navigation.goBack()}>
									<Icon name="back" />
								</Button>
							</Left>

							<Body>
								<Title>Category</Title>
								<Subtitle>Topoľčany</Subtitle>
							</Body>

							<Right>
								<Button>
								    <Icon name="location" colorActive />
								</Button>
							</Right>
				      	</Header>

				      	<ModuleCategoriesListHorizontal
				      		navigate={ navigate }
				      		screen={"SubcategoryScreen"}
				            items={ subcategories } 
				      	/>

  					</View>


  					<View pbx5 bgColorWhite>

                        <ModuleEventListVertical
	                    	items={ events } />

                    </View>

                </ModuleHeaderScroll>

        	</Container>
        
        );

    }

}