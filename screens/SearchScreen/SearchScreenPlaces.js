import React, { Component } from 'react';

// Components
import { 
    Content,
    Title,
    Subtitle,
    Left,
    Right,
    Touchable,
    Thumbnail,
    View
} 
from './../../components';


export default class SearchScreenPlaces extends Component {

    render() {
        
        return (
          
            <Content separator>

                <Touchable itemImageText>
                    <Left touchablePoster>
                        <Thumbnail source={require('./../../assets/images/1.jpg')} />
                    </Left>

                    <Right>
                        <Title numberOfLines={1}>Ministry of Fun</Title>
                        <Subtitle>Banská Bstrica - Lorem Ipsum</Subtitle>
                    </Right>
                </Touchable>


                <View separator />


                <Touchable itemImageText>
                    <Left touchablePoster>
                        <Thumbnail source={require('./../../assets/images/2.jpg')} />
                    </Left>

                    <Right>
                        <Title>Next place</Title>
                        <Subtitle>Great place in Hootewille</Subtitle>
                    </Right>
                </Touchable>


                <View separator />


                <Touchable itemImageText>
                    <Left touchablePoster>
                        <Thumbnail source={require('./../../assets/images/3.jpg')} />
                    </Left>

                    <Right>
                        <Title>Lorem ipsum amet</Title>
                        <Subtitle>This is great subtitle</Subtitle>
                    </Right>
                </Touchable>

            </Content>
        
        );

    }

}