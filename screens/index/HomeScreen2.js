import React, { Component } from 'react';

// Components
import {
    Container, 
    Header, 
    Left, 
    Right, 
    Body, 
    Button, 
    Icon,
    Text, 
    Title, 
    Subtitle,
    View,
} from './../../components';


// Modules
import {
    ModuleEventListHorizontal,
    ModuleCategoriesListHorizontal,
    ModuleHeaderScroll,
} from './../../components/modules';


// Fake Data
import {
    categories,
    events
} from './../../assets/fakeData';


export default class HomeScreen extends Component {

    render() {

        const navigate = this.props.navigation.navigate;
        
        return (

            <Container>

                <ModuleHeaderScroll>

                    <View>

                        <Header hasSubtitle>
                            <Left>
                                <Button>
                                    <Icon name="invite-friend" />
                                </Button>
                            </Left>

                            <Body>
                                <Title>Browse</Title>
                                <Subtitle>Topoľčany</Subtitle>
                            </Body>

                            <Right>
                                <Button>
                                    <Icon name="location" colorActive />
                                </Button>
                            </Right>
                        </Header>


                        <ModuleCategoriesListHorizontal
                                screen={"CategoryScreen"}
                                navigate={ navigate }
                                items={ categories }
                        />

                    </View>


                    <View pbx10 bgColorWhite>

                        <ModuleEventListHorizontal
                            navigate={ navigate }
                            type="category"
                            content="Parties" 
                            items={ events } 
                            style={{ marginTop:27 }} />


                        <View mbx12 />


                        <ModuleEventListHorizontal
                            navigate={ navigate }
                            type="category"
                            content="Festivals" 
                            items={ events } />


                        <View mbx12 />
                        

                        <ModuleEventListHorizontal
                            navigate={ navigate }
                            type="category"
                            content="Concerts" 
                            items={ events } />

                    </View>

                </ModuleHeaderScroll>

            </Container>
        
        );

    }
}
