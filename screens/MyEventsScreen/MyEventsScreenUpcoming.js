import React, { Component } from 'react';

// Components
import { 
    Content,
    Section,
    Touchable,
    View,
    Left,
    Right,
    Title,
    Subtitle,
    Text,
    Thumbnail,
    Image,
} 
from './../../components';


export default class MyEventsScreenUpcoming extends Component {

    render() {
        
        return (
          
            <Content separator>

                <Touchable itemImageText>
                    <Left touchablePoster>
                        <Thumbnail source={{ uri:"https://cdn-az.allevents.in/banners/1673613c68141de78df7f10f44457f05" }} />
                    </Left>

                    <Right>
                        <Title numberOfLines={1}>6 x DJ Contest - SOLID Festival 2017 ●</Title>
                        <Subtitle>16 Jan, Mon • Belgium lorem ipsum</Subtitle>
                    </Right>
                </Touchable>


                <View separator />


                <Touchable itemImageText>
                    <Left touchablePoster>
                        <Thumbnail source={{ uri:"https://cdn-az.allevents.in/banners/014ce0d4f8854965b529fa96d827b49f" }} />
                    </Left>

                    <Right>
                        <Title>Tomorrowland Belgium 2017</Title>
                        <Subtitle>16 Jan, Mon • Belgium lorem ipsum</Subtitle>
                    </Right>
                </Touchable>


                <View separator />


                <Touchable itemImageText>
                    <Left touchablePoster>
                        <Thumbnail source={{ uri:"https://cdn-az.allevents.in/banners/b942596e74587cb9629b904ef70477df" }} />
                    </Left>

                    <Right>
                        <Title>SOLID Festival 2017</Title>
                        <Subtitle>16 Jan, Mon • Belgium lorem ipsum</Subtitle>
                    </Right>
                </Touchable>

            </Content>
        
        );

    }

}