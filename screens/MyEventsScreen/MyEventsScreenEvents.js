
import React, { Component } from 'react';

// ReactNative components
import { FlatList } from 'react-native';

// Components
import { 
  	Content,
    Section,
    Left,
    Right,
  	Title,
  	Subtitle,
    Text,
    Thumbnail,
    Image,
    Touchable,
    Button,
    View
} 
from './../../components';

// Modules
import { ModuleEventListHorizontal } from './../../components/modules';

// Fake Data
import { events } from './../../assets/fakeData';


/*
export default class SearchEvents extends Component {

	renderItem( data ) {

		<CardItem 
			data={ data } />

	}

    render() {
        
        return (
          
            <View>
                <FlatList
                    data={this.props.items}
                    keyExtractor={items => items.text}
                    renderItem={data => this.renderItem(data.item)}
                    ListHeaderComponent={this.renderSeparator}
                    ListFooterComponent={this.renderSeparator}
                    ItemSeparatorComponent={this.renderSeparator}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                />
            </View>
        
        );

    }
    
}
*/


export default class MyEventsScreenEvents extends Component {

	render() {
        
        return (
          
          	<Content background>


                <View separator />


                <Section boxSection>
                    <View sectionHeader>
                        <Title fontSizeH6 fontWeightBold>Upcoming events</Title>

                        <Button active>
                            <Text uppercase={false}>See all</Text>
                        </Button>
                    </View>

                    <Touchable itemImageText>
                        <Left touchablePoster>
                            <Thumbnail source={{ uri:"https://cdn-az.allevents.in/banners/1673613c68141de78df7f10f44457f05" }} />
                        </Left>

                        <Right>
                            <Title fontSizeH6>Tomorrowland Belgium 2017</Title>
                            <Subtitle>16 Jan, Mon • Belgium lorem ipsum</Subtitle>
                        </Right>
                    </Touchable>


                    <View separator />


                    <Touchable itemImageText>
                        <Left touchablePoster>
                            <Thumbnail source={{ uri:"https://cdn-az.allevents.in/banners/b942596e74587cb9629b904ef70477df" }} />
                        </Left>

                        <Right>
                            <Title fontSizeH6>SOLID Festival 2017</Title>
                            <Subtitle>16 Jan, Mon • Belgium lorem ipsum</Subtitle>
                        </Right>
                    </Touchable>
                </Section>


                <View separator />


                <Section boxSection2>
                    <ModuleEventListHorizontal 
                        type="author"
                        content={{
                            image:require("./../../assets/images/1.jpg"),
                            text:"Barracuda Club..."
                        }}
                        items={ events } />


                    <View separatorX />


                    <ModuleEventListHorizontal 
                        type="author"
                        content={{
                            image:require("./../../assets/images/4.jpg"),
                            text:"Tiesto"
                        }}
                        items={ events } />
                </Section>

	        </Content>
        
        );

    }

}
