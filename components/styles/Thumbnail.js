
export default (variables) => {
	const platform = variables.platform;

  	const thumbnailTheme = {
		width: 50,
		height: 50,
		borderRadius: 25,

		".sizex1": {

			width: 30,
			height: 30,
			borderRadius: 15

		}
	};
    

  return thumbnailTheme;
};
